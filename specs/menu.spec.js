const homePage = require('../pages/home')
const aboutPage = require('../pages/about')
const contactPage = require('../pages/contact')

describe('Menu navigation', function () {
  describe('from the homepage', function () {
    beforeEach('user goes to the home page', async function () {
      await homePage.open()
    })

    it('clicks the about link and goes to the about page', async function () {
      let page = await homePage.isCurrentPage()
      page.should.be.true
      await homePage.topNavItems.about.element.click()
      page = await aboutPage.isCurrentPage()
      page.should.be.true
    })

    it('clicks the contact link and goes to the contact page', async function () {
      let page = await homePage.isCurrentPage()
      page.should.be.true
      await homePage.topNavItems.contact.element.click()
      page = await contactPage.isCurrentPage()
      page.should.be.true
    })
  })

  describe('from the about page', function () {
    beforeEach('user goes to the about page', async function () {
      await aboutPage.open()
    })

    it('clicks the home link and goes to the home page', async function () {
      let page = await aboutPage.isCurrentPage()
      page.should.be.true
      await aboutPage.topNavItems.home.element.click()
      page = await homePage.isCurrentPage()
      page.should.be.true
    })

    it('clicks the contact link and goes to the contact page', async function () {
      let page = await aboutPage.isCurrentPage()
      page.should.be.true
      await aboutPage.topNavItems.contact.element.click()
      page = await contactPage.isCurrentPage()
      page.should.be.true
    })
  })

  describe('from the contact page', function () {
    beforeEach('user goes to the contact page', async function () {
      await contactPage.open()
    })

    it('clicks the home link and goes to the home page', async function () {
      let page = await contactPage.isCurrentPage()
      page.should.be.true
      await contactPage.topNavItems.home.element.click()
      page = await homePage.isCurrentPage()
      page.should.be.true
    })

    it('clicks the about link and goes to the about page', async function () {
      let page = await contactPage.isCurrentPage()
      page.should.be.true
      await contactPage.topNavItems.about.element.click()
      page = await aboutPage.isCurrentPage()
      page.should.be.true
    })
  })
})

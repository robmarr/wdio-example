const homePage = require('../pages/home')

describe('Home page', function () {
  before('user goes to the home page', async function () {
    await homePage.open()
  })

  it('has the correct title', function () {
    homePage.title.should.endWith('home')
  })

  it('has the menu displayed correctly', async function () {
    const selected = await homePage.topNavSelected.getText()
    selected.should.be.string('home')
    homePage.topNavItems.should.have.all.keys('about', 'contact')
    homePage.topNavItems.about.href.should.be.string('/about')
    homePage.topNavItems.contact.href.should.be.string('/contact')
  })

  it('has the home page content', async function () {
    const content = await homePage.content.getText()
    content.should.startWith('this is the home page')
  })
})

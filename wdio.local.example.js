module.exports = {
  maxInstances: 10,
  waitforTimeout: 60000,
  host: 'localhost',
  capabilities: [
    {
      browserName: 'chrome',
      'goog:chromeOptions': {
        args: ['--headless']
      }
    },
    {
      browserName: 'firefox',
      'moz:firefoxOptions': {
        args: ['-headless']
      }
    }
  ],
  services: ['selenium-standalone'],
  mochaOpts: {
    timeout: 60000
  }
}

const Page = require('./page')

class HomePage extends Page {
  async content () {

  }

  constructor () {
    super()
    this.name = 'home-page'
    this.url = '/'
  }

  async open () {
    await super.open(this.url)
    const [content] = await Promise.all([this.main.$('p')])
    Object.assign(this, { content })
  }
}

module.exports = new HomePage()

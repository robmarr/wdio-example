const { URL } = require('url')

class Page {
  constructor () {
    this.url = '/'
  }

  async isCurrentPage () {
    const url = new URL(await browser.getUrl())
    return url.pathname === this.url
  }

  async open () {
    await browser.url(`${this.url}`)
    const [title, main, header, heading, topNav, topNavSelected, topNavItems] = await Promise.all([
      browser.getTitle(),
      $('main'),
      $('header'),
      $('header h1'),
      $('header nav'),
      $('header nav span'),
      $$('header nav a')
        .then(async navLinks => {
          const links = {}
          await Promise.all(
            navLinks.map(
              element => Promise.all([
                element.getText(),
                element.getAttribute('href')
              ]).then(([text, href]) => { links[text] = { href, element } })
            ))
          return links
        })
    ])
    Object.assign(this, { title, main, header, heading, topNav, topNavSelected, topNavItems })
    await main.waitForDisplayed()
  }
}

module.exports = Page

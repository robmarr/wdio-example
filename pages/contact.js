const Page = require('./page')

class ContactPage extends Page {
  constructor () {
    super()
    this.name = 'contact-page'
    this.url = '/contact'
  }

  async open () {
    await super.open(this.url)
    const [content] = await Promise.all([this.main.$('p')])
    Object.assign(this, { content })
  }
}

module.exports = new ContactPage()

const Page = require('./page')

class AboutPage extends Page {
  constructor () {
    super()
    this.name = 'about-page'
    this.url = '/about'
  }

  async open () {
    await super.open(this.url)
    const [content] = await Promise.all([this.main.$('p')])
    Object.assign(this, { content })
  }
}

module.exports = new AboutPage()
